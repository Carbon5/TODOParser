#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <map>

#define MINBUFF 10
#define TABSIZE 4
#define MAXWIDH 60

#define IMMIDTEXT "\033[30;41m"
#define IMPNTTEXT "\033[1;31m"
#define UPNXTTEXT "\033[1;33m"
#define CLEARTEXT "\033[21;0m"

enum todoType {
    lineStart,
    blockContd,
    noneTODO
};

enum importanceLevel {
    notImptnt,
    urgent,
    immidiate,
    takenote
};

std::string bypassSpaces(std::string in) {
    int p;
    for (p = 0; in[p] == ' '; p++);
    return in.substr(p);
}

// TODO!: Move where the iportance level is located in the todo indicator
todoType getTodoType(std::string line, importanceLevel& impt, std::string& content) {
    int p = line.find("//");
    if (p == std::string::npos) return noneTODO;

    line = line.substr(p+2); // remove the comment mark and everything before it
    int thetodo = line.find("TODO");
    if (thetodo == 0 || thetodo == 1) {
        line = line.substr(thetodo + 4); // remove the TODO
        if (line[0] == '!') { // is this an Important TODO?
            line = line.substr(1);
            impt = urgent;
        } else if (line[0] == '+'){
            line = line.substr(1);
            impt = takenote;
        } else if (line[0] == '^'){
            line = line.substr(1);
            impt = immidiate;
        } else {
            impt = notImptnt;
        }

        if (line[0] == ':') {
            content = " " + bypassSpaces(line.substr(1));
            return lineStart;
        } else {
            return noneTODO;
        }
    } else if (line[0] == '>') {
        content = " " + bypassSpaces(line.substr(1));
        return blockContd;
    }
    return noneTODO;
}

std::string formatFileName(std::string dir, int lineN) {
    size_t p = dir.rfind('/');
    if (p != std::string::npos) dir = dir.substr(p+1);

    dir += ':' + std::to_string(lineN);

    return dir;
}

std::string formatTODOLeadin(std::string flnum, int buff) {
    flnum += "|";
    while (flnum.size() < buff-2) flnum += "-";
    flnum += ">";
    return flnum;
}

std::string wordWrap(std::string content, int flnamesz, int width) {
    std::string tab = "\n";
    while (tab.size() < flnamesz + TABSIZE + 1) tab += " ";


    int at = width;
    width -= TABSIZE;
    while (at < content.size()) {
        size_t p = content.rfind(' ',at);
        if (at - p > width) { // break this word if it is longer than the width
            content.insert(at,tab);
            at += tab.size() + width;
        } else {
            content.replace(p,1,tab);
            at = p + tab.size() + width;
        }
    }
    return content;
}

int main (int argc, char** argv) {
    if (argc == 1) {
        std::cout << "Usage: todo filename0 filename1 ...\n";
        return 1;
    }

    std::string problems = "";

    std::map<std::string,std::map<int,std::pair<importanceLevel,std::string>>> todos;

    // Read-in from all the files and compile the
    // map of todo content
    for (int argNum = 1; argNum < argc; argNum++) {
        std::ifstream input(argv[argNum]);
        if (!input) { // I dont want errors in the middle of the todo stuff
            // this way we can print the errors at the end
            problems += "Unable to open " + std::string(argv[argNum]) + '\n';
            continue;
        }

        todos.emplace(argv[argNum],std::map<int,std::pair<importanceLevel,std::string>>());
        auto& thismap = todos.at(argv[argNum]);

        std::string line;
        long int lnum = 0;
        bool onBlock = false;
        int currtodo;
        importanceLevel importance = notImptnt;
        while (getline(input, line)) {
            lnum++;

            // TODO: remove bug where empty lines add
            //>      an extra space in the content
            std::string content;
            switch (getTodoType(line, importance, content)) {
                case lineStart : {
                    currtodo = lnum;
                    thismap.emplace(currtodo, std::make_pair(importance,content));
                    onBlock = true;
                    break;
                }
                case blockContd : {
                    if (!onBlock) break;
                    thismap.at(currtodo).second += content;
                    break;
                }
                case noneTODO :
                default : {
                    onBlock = false;
                }
            }
        }

        input.close();
    }

    // get the length of the longest name:linenum
    std::vector<std::string> filenms;
    for (auto& mit : todos)
        for (auto& num : mit.second)
            filenms.emplace_back(formatFileName(mit.first,num.first));
    int buff = MINBUFF;
    for (auto& vit : filenms)
        buff = (vit.size() > buff) ? vit.size() : buff;
    buff += 4;

    // take the map and print out the content "nicly"
    for (auto& mit : todos) for (auto& lit : mit.second) {
        const std::string&     filen = mit.first;
        const int&             lnnum = lit.first;
        const importanceLevel& level = lit.second.first;
        const std::string&     contt = lit.second.second;
        std::cout << ((level == immidiate) ? IMMIDTEXT :
                      (level == urgent) ? IMPNTTEXT :
                      (level == takenote) ? UPNXTTEXT : CLEARTEXT)
                  << formatTODOLeadin(formatFileName(filen,lnnum),buff)
                  << ((level == immidiate) ? std::string(CLEARTEXT) + IMPNTTEXT : "")
                  << wordWrap(contt,buff,MAXWIDH)
                  << CLEARTEXT << std::endl;
    }

    std::cout << CLEARTEXT << problems << std::endl;

    return 0;
}
