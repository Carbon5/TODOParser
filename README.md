# Readme

Simple script for listing TODOs in files that use C/C++ style comments.

## Usage

`$> todo filname0 filename1 ...`

## One File Example:
`$> todo testFile`

**input from file:**

```c++
Nothing matters before the todo // TODO: Only what appears after the start of the comment is ever shown in the parsed output

Valid starts to a todo are as follows:
//TODO: No Space needed before the TODO
// TODO: There can be one though
// TODO: Importance level 0
// TODO+: Importance level 1
// TODO!: Importance level 2
// TODO^: Importance level 3

// TODO:           Leading spaces after the todo keyword are fine
//  TODO: Excess Leading spaces before the todo keyword are not

Note: The regex for a valid todo is basically '// ?TODO[\+!\^]?: *'


There can be text in front of block TODOs as well   // TODO+: Multiline TODOs are formmated similarly.
                                                    //> All of the folowing
                                                    //> lines of the block MUST start with "//>".
                                                    //>       Leading spaces are still ignored on all lines,
                                                    //>
                                                    //> blank lines are allowed (but will cause a space to be added to the text),
                                            //> and the block does not require unified indentation.
                                                    //> There is no closing bracket for the todo text. It continues parsing until there are
                                                    //> no more lines in the block.

//> Lines that are not connected to a block start are ignored

// TODO: All TODOs are word-wraped automatically. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam finibus augue sed nunc volutpat mattis. Donec nec lobortis ante. Donec vitae sagittis purus. Nulla erat urna, accumsan sed mollis et, gravida cursus orci.

// TODO: The word-wrapping works with very llllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllong single words as well
```

**output:**
```text
testFile:1|--> Only what appears after the start of the comment is ever
                   shown in the parsed output
testFile:4|--> No Space needed before the TODO
testFile:5|--> There can be one though
testFile:6|--> Importance level 0
testFile:7|--> Importance level 1
testFile:8|--> Importance level 2
testFile:9|--> Importance level 3
testFile:11|-> Leading spaces after the todo keyword are fine
testFile:17|-> Multiline TODOs are formmated similarly. All of the
                   folowing lines of the block MUST start with "//>".
                   Leading spaces are still ignored on all lines,  blank
                   lines are allowed (but will cause a space to be added to
                   the text), and the block does not require unified
                   indentation. There is no closing bracket for the todo
                   text. It continues parsing until there are no more lines
                   in the block.
testFile:29|-> All TODOs are word-wraped automatically. Lorem ipsum dolor
                   sit amet, consectetur adipiscing elit. Etiam finibus
                   augue sed nunc volutpat mattis. Donec nec lobortis ante.
                   Donec vitae sagittis purus. Nulla erat urna, accumsan
                   sed mollis et, gravida cursus orci.
testFile:31|-> The word-wrapping works with very
                   llllllllllllllllllllllllllllllllllllllllllllllllllllllll
                   llllllllllllllllllllllllllllllllllllllllllllllllllllllll
                   llllllllllllllllllllllllllllllllllllllllllllong single
                   words as well
```
Note: The output is colored based on importance level:
- level 0 has no color
- level 1 is yellow
- level 2 is red
- level 3 is also red but the filname has inverted text and background colors

## Use with find for a project

`$> find -type f -regex ".*\.\(cpp\|h\)" -not -path "./reference/*" -exec todo {} +`
<p>That finds all the cpp and h files in all subdirectories except for ./reference/ and
passes them into todo. todo then goes through all the files and prints out the formmated
todo list.

## Installation

`$> sudo make install`

This compiles it to `todo` and then moves the binary file to `/usr/local/bin/todo`.
